package com.muhardin.endy.training.git.bukualamat;

import com.muhardin.endy.training.git.bukualamat.dao.TamuDao;
import com.muhardin.endy.training.git.bukualamat.entity.Tamu;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(scripts = {"classpath:delete-tamu.sql", "classpath:sample-tamu.sql"})
public class BukuAlamatApplicationTests {

	@Autowired private TamuDao tamuDao;
	@Autowired private DataSource dataSource;

	@Test
	public void querySemuaDataTamu() {
		Page<Tamu> hasilQuery
				= tamuDao.findAll(PageRequest.of(0, 10));

		/*
		System.out.println("Jumlah data dalam database : "+hasilQuery.getTotalElements());
		for(Tamu t : hasilQuery.getContent()){
			System.out.println("Nama : "+t.getNama());
			System.out.println("Email : "+t.getEmail());
		}
		*/

		Assert.assertEquals(5, hasilQuery.getTotalElements());

		String nama = "user001";
		String email = "user001@coba.com";

		boolean ketemu = false;
		for(Tamu t : hasilQuery.getContent()){
			if (t.getNama().equals(nama)) {
				ketemu = true;
			}
		}

		Assert.assertTrue(ketemu);
	}

	@Test
	public void testSimpanDataTamu() throws Exception {
		Assert.assertTrue(tamuDao.count() == 5);

		Tamu t = new Tamu();
		t.setNama("User Test");
		t.setEmail("usertest001@coba.com");

		tamuDao.save(t);

		Assert.assertTrue(tamuDao.count() == 6);

		String sql = "select * from tamu where nama = ?";
		PreparedStatement ps = dataSource.getConnection().prepareStatement(sql);
		ps.setString(1, "User Test");
		ResultSet res = ps.executeQuery();

		Assert.assertTrue(res.next());
		String nama = res.getString("nama");
		String email = res.getString("email");

		Assert.assertEquals("User Test",nama);
		Assert.assertEquals("usertest001@coba.com",email);

		res.close();
	}

}
